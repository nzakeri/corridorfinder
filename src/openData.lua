require 'lfs'
require 'io'
require 'gd'
require 'math'

offsetx = 256;
offsety = 256;
scale = 50;
displayWidth = 512;
displayHeight = 512;

im = gd.createTrueColor(displayWidth, displayHeight);
black = im:colorAllocate(0, 0, 0)
green = im:colorAllocate(0, 255, 0)
phosphorusGreen = im:colorAllocate(51, 255, 51)
white = im:colorAllocate(255, 255, 255)
red = im:colorAllocate(255, 0, 0)
corridorWidth = 0.5 -- half the width of the corridor
negligableDistance = 0.05 -- accepted distance between a point and a junction. less than this is acceptable

function drawJunction (junc)
	local px = junc.px*scale + offsetx; --center point of the junction
	local py = junc.py*scale + offsety;
	local n1x = junc.n1x; -- normal of the junction
	local n1y = junc.n1y;
	local n2x = junc.n2x;
	local n2y = junc.n2y;
	local sizen1 = math.sqrt(n1x*n1x + n1y*n1y)
	local sizen2 = math.sqrt(n2x*n2x + n2y*n2y)
	local ex = px + n1x*scale/sizen1;
	local ey = py + n1y*scale/sizen1;
	local ex2 = px + n2x*scale/sizen2;
	local ey2 = py + n2y*scale/sizen2;

	im:line(px,py,ex,ey,phosphorusGreen);
	im:line(px,py,ex2,ey2,phosphorusGreen);

end



function drawPoint( point,color)
	im:setPixel(point.x*scale + offsetx,point.y*scale+offsety,color)
--	print ("drawing at", point.x*scale + offsetx,point.y*scale + offsety)
end

function drawAllPoints(points)
	print ("hi I am drawing")
	im:filledRectangle(0, 0, displayWidth, displayHeight, black)


	for k,v in pairs(points) do
		drawPoint (v,white)
	end
	im:png("test.png")
end

function drawLine (line,color)
	local newc = scale*line.c-offsetx*line.a-offsety*line.b;
	local eligPoints = {};

	local y0 = -newc/line.b; -- if x = 0;
	if y0 > 0 and y0 <= displayHeight then table.insert(eligPoints,{0,y0}) end

	local ymax = (-displayWidth*line.a-newc)/line.b;
	if ymax > 0 and ymax <= displayHeight then table.insert(eligPoints,{displayHeight,ymax}) end

	local x0 = -newc/line.a; -- if y = 0;
	if x0 > 0 and x0 <= displayWidth then table.insert(eligPoints,{x0,0}) end

	local xmax = (-displayHeight*line.b-newc)/line.a;
	if xmax > 0 and xmax <= displayWidth then table.insert(eligPoints,{xmax,displayHeight}) end

	if table.getn(eligPoints) == 2 then
		im:line(eligPoints[1][1],eligPoints[1][2],eligPoints[2][1],eligPoints[2][2],color)
	else
		print ("a line was draw but was outside")
	end
end

numiterations = 400
seed = 5;
lineInclusionThreshold = 0.01

minNumberOfPoints = 10
pointAcceptThreshold = 0.0000001 -- we want to leave out the points that are located near the center.

maxSubLoopIter = 10

function pruneZeroPoints(points) -- this function removes all points that are at the center.
	local newPoints = {};
	for key,point in pairs (points) do
		if point.x > pointAcceptThreshold or point.y > pointAcceptThreshold or point.z > pointAcceptThreshold or
			point.x < -pointAcceptThreshold or point.y < -pointAcceptThreshold or point.z < -pointAcceptThreshold
			then
			table.insert(newPoints,point);
		end
	end
	return newPoints;
end

function getAllLines(points)
	local allPointsExceptZeros = pruneZeroPoints(points)

	local bestAcceptedPointValues = allPointsExceptZeros
	local bestAcceptedPointKeys = {};
	local lines = {}
	local bestline;
	local bestInsideCount = 0;
	local subLoopCounter = 0;
	math.randomseed(seed)
	for i=1,numiterations,1 do
		--choose two points randomly
		local p1 = math.random(table.getn(bestAcceptedPointValues ))
		local p2 = math.random(table.getn(bestAcceptedPointValues )-1)
		if p2 >= p1 then p2 = p2+1 end
		p1 = bestAcceptedPointValues[p1]
		p2 = bestAcceptedPointValues[p2]
		--calculate line
		local l = {}
		l.b = p1.x-p2.x;
		l.a = -(p1.y-p2.y);
		l.c = -l.a*p1.x-l.b*p1.y;

		--drawLine (l,green);
		--drawPoint (p1,white)
		--drawPoint (p2,white)

		--see howmany points lie on line
		local insideCount = 0
		local distScale = math.sqrt(l.b*l.b+l.a*l.a);
		local acceptedPointsKeys = {} -- these points are the ones that fit inside the line
		local acceptedPointsValues = {} -- these points are the ones that fit inside the line

		for key,point in pairs(allPointsExceptZeros) do
			local distance = math.abs(l.a*point.x + l.b*point.y + l.c)/distScale;
			if distance < lineInclusionThreshold then
				insideCount = insideCount + 1;
				table.insert(acceptedPointsKeys,key,key);
				table.insert(acceptedPointsValues,point)
			end
		end

		if (insideCount > minNumberOfPoints) and insideCount > bestInsideCount then -- we are improving
				subLoopCounter = 0;
				bestAcceptedPointValues = acceptedPointsValues;
				bestAcceptedPointKeys = acceptedPointsKeys;
				bestInsideCount=insideCount
				bestLine = l;
		end

		--drawAllPoints(bestAcceptedPointValues)
		--drawAllPoints(allPointsExceptZeros)
		if subLoopCounter == maxSubLoopIter and bestInsideCount ~= 0 then -- we have searched enough add the best line
			table.insert(lines,bestLine);
			local newTable = {};
			for key,value in pairs(allPointsExceptZeros) do
				if bestAcceptedPointKeys [key] == nil then
					table.insert(newTable,value);
				end
			end
			allPointsExceptZeros = newTable;

			if (table.getn(allPointsExceptZeros) < minNumberOfPoints) then
				break;
			end
			bestAcceptedPointValues = allPointsExceptZeros
			bestInsideCount = 0;
			subLoopCounter = 0;
		end


		subLoopCounter=subLoopCounter+1;

    end
	return lines
end

function getJunction (points,lines)
	local maxPointsMatched = 0
	local currentBestParams
	for k1,l1 in pairs(lines) do -- search all pairs of lines and for each pair find the junction passing though the line. Then count how many points agree with this junction. then choose the best junction
		for k2,l2 in pairs (lines) do
			if (l1 == l2 ) then break end
			local sizen1 = math.sqrt(l1.a*l1.a + l1.b*l1.b)
			local sizen2 = math.sqrt(l2.a*l2.a + l2.b*l2.b)

			local deter = (l1.a*l2.b-l2.a*l1.b)
			local normalizeddeter = math.abs(deter/sizen1/sizen2);
			if (normalizeddeter < 0.0001) then
				break
			end

			local px = (-l2.b*l1.c + l1.b*l2.c)/deter;
			local py = (l2.a*l1.c - l1.a*l2.c)/deter;
			--normal khate aval:
			local n1x = l1.a;
			local n1y = l1.b;
			--normal khate dovom:
			local n2x = l2.a;
			local n2y = l2.b;

			local dirl1 = n1x*px+n1y*py;
			local dirl2 = n2x*px+n2y*py;
			-- flip the normals if they are facing towards the wall
			if (dirl1 > 0) then n1x = -n1x; n1y = -n1y end;
			if (dirl2 > 0) then n2x = -n2x; n2y = -n2y end;
			-- calculate the constants for the two lines running throught the middle of the junction
			local c1 = l1.c - corridorWidth*math.sqrt(n1x*n1x+n1y*n1y);
			local c2 = l2.c - corridorWidth*math.sqrt(n2x*n2x+n2y*n2y);
			local distScale1 = math.sqrt(l1.b*l1.b+l1.a*l1.a);
			local distScale2 = math.sqrt(l2.b*l2.b+l2.a*l2.a);
			local count = 0;
			-- chech distance of each point to the junction represented by the normals n1, n2 and p. This is also represented by the two crossing lines (n1,c1) and (n2,c2).
			for k3,p in pairs(points) do
				local dist1 = math.abs(n1x*p.x + n1y*p.y + c1)/distScale1;
				local dist2 = math.abs(n2x*p.x + n2y*p.y + c2)/distScale2;
				local dist1 = dist1 - corridorWidth
				local dist2 = dist2 - corridorWidth
				if (dist1 < dist2 and dist1 < negligableDistance and dist1 > -negligableDistance) or
					(dist2 < dist1 and dist2 < negligableDistance and dist2 > -negligableDistance)
					then
					count =count + 1;
				end
			end
			if (count > maxPointsMatched) then
				maxPointsMatched = count;
				currentBestParams = {px=px,py=py,n1x=n1x,n1y=n1y,n2y=n2y,n2x=n2x}
			end

		end
	end

	if (currentBestParams == nil) then return end
	local sizeofn1 = math.sqrt(currentBestParams.n1x*currentBestParams.n1x + currentBestParams.n1y*currentBestParams.n1y)
	local sizeofn2 = math.sqrt(currentBestParams.n2x*currentBestParams.n2x + currentBestParams.n2y*currentBestParams.n2y)
	return {px=(currentBestParams.px+currentBestParams.n1x*corridorWidth/sizeofn1+currentBestParams.n2x*corridorWidth/sizeofn2),
		py=(currentBestParams.py+currentBestParams.n1y*corridorWidth/sizeofn1+currentBestParams.n2y*corridorWidth/sizeofn2),
		n1x=currentBestParams.n1x,n1y=currentBestParams.n1y,n2y=currentBestParams.n2y,n2x=currentBestParams.n2x}

end

f = io.open("data/onelineOfScan.txt")
s = f :read()
points = {}
--y = {}
--z = {}
i = 0
for x1,y1,z1 in string.gmatch(s, "([^%s]+)%s([^%s]+)%s([^%s]+)%s,") do

	i = i+1
	points[i] = {x=tonumber(x1),y=tonumber(y1),z=tonumber(z1)}
	--y[i] = y1
	--z[i] = z1
	--print(points[i].y)
	end
--for k,v in pairs(x) do print(k,v) end
-- getting size of a table
--print(table.getn(x))
--print(table.getn(y))
--print(table.getn(z))


for key,value in pairs(points) do
	drawPoint(value,red)
end


local lines = getAllLines(points);
print (table.getn(lines))
for key,line in pairs(lines)do
	drawLine(line,white)
end

local junction = getJunction (points,lines);

if (junction == nil) then print ("no junction found for drawing") else
	drawJunction(junction)
end
im:png("output.png")

--for i in
--im:setPixel(

f:close()
